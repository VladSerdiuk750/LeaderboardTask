I created a simple leaderboard popup that reads a local JSON file in the Unity project from the Resources folder, parses the data, and displays it, along with player avatars that are downloaded as needed.

## Features

- Created a scene with button that triggers the opening of the leaderboard popup.
- The popup built using Unity's UI primitives and should fit well on all devices with a flexible scale(was taken mobile portrait devices as base).
- The popup contain a list view that shows the player's name, score, avatar, and player type.
- Player type is indicated by color and size (Diamond, Gold, Silver, Bronze, Default).
- Avatar of the player is loading from internet and while loading show "Loading" message on avatar place.
- Load Player avatars after popup opened, during loading show "Loading" message on avatar place. Avatars caching will be considered a plus but not required.
- Reading Json file from Resouces was using JSONReader.
- The leaderboard popup contains the close button which closes the popup.
- For accessing to Managers used GameManager with Singleton pattern.
- To `PopupManager` was added overloading that takes additional parent parameter to instantiate it on canvas and not right on the scene.
