using System.Collections.Generic;

/// <summary>
/// Converts string values to enum
/// </summary>
public static class ConvertToEnumHelper
{
    /// <summary>
    /// Map strings to enum values
    /// </summary>
    private static Dictionary<string, LeaderboardEntityType> _leaderboardEntityKindMap = new Dictionary<string, LeaderboardEntityType> {
      {
      LeaderboardEntityType.Default.ToString(),
      LeaderboardEntityType.Default
      },
      {
      LeaderboardEntityType.Bronze.ToString(),
      LeaderboardEntityType.Bronze
      },
      {
      LeaderboardEntityType.Silver.ToString(),
      LeaderboardEntityType.Silver
      },
      {
      LeaderboardEntityType.Gold.ToString(),
      LeaderboardEntityType.Gold
      },
      {
      LeaderboardEntityType.Diamond.ToString(),
      LeaderboardEntityType.Diamond
      }
    };

    /// <summary>
    /// Converts string to LeaderboardEntityType
    /// </summary>
    /// <param name="value"></param>
    /// <param name="defaultValue"></param>
    /// <returns></returns>
    public static LeaderboardEntityType ToEnum(this string value, LeaderboardEntityType defaultValue)
    {
        if (!_leaderboardEntityKindMap.TryGetValue(value, out LeaderboardEntityType leaderboardEntityType))
        {
            return defaultValue;
        }
        return leaderboardEntityType;
    }
}
