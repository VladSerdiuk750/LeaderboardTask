using UnityEngine;

/// <summary>
/// Generic Singleton class that ensures only one instance of a MonoBehaviour can exist in the scene.
/// </summary>
/// <param name="T">The type of the Singleton instance.</param>
public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T _instance;

    /// <summary>
    /// Gets the instance of the Singleton.
    /// </summary>
    public static T Instance
    {
        get
        {
            if (_instance is null)
            {
                _instance = FindObjectOfType<T>();
            }
            else if (_instance != FindObjectOfType<T>())
            {
                Destroy(FindObjectOfType<T>());
            }

            DontDestroyOnLoad(FindObjectOfType<T>());

            return _instance;
        }
    }

    /// <summary>
    /// Checks if the Singleton instance has been initialized.
    /// </summary>
    public static bool IsInitialized => _instance != null;

    /// <summary>
    /// Initializes singleton object on awake
    /// </summary>
    protected virtual void Awake()
    {
        if (_instance != null)
        {
            Debug.LogError($"[Singleton] Trying to instantiate a second instance of singleton class {GetType().Name}");
        }
        else
        {
            _instance = (T)this;
        }
    }

    /// <summary>
    /// Destroys an instance
    /// </summary>
    protected virtual void OnDestroy()
    {
        if (_instance == this)
        {
            _instance = null;
        }
    }
}