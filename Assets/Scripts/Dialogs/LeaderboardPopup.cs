using SimplePopupManager;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class LeaderboardPopup : MonoBehaviour, IPopupInitialization
{
    /// <summary>
    /// Container of leaderboard entities in scroll view
    /// </summary>
    [SerializeField]
    private Transform _entitiesContainer;

    /// <summary>
    /// Name of popup prefab in addressables
    /// </summary>
    private string _prefabName = "LeaderboardPopup";

    /// <summary>
    /// Prefab name of leaderboard entity to load it from resources
    /// </summary>
    private string _leaderboardEntityPrefabName = "LeaderboardEntity";

    /// <summary>
    /// Field for saving loaded prefab of leaderboard entity
    /// </summary>
    private GameObject _leaderboardEntity;

    /// <summary>
    /// Initiates leaderboard popup
    /// </summary>
    /// <param name="parameters">Data for leaderboards entities</param>
    /// <returns></returns>
    public Task Init(object parameters)
    {
        List<LeaderboardEntity> leaderboard = parameters as List<LeaderboardEntity>;
        LoadLeaderboardEntity();
        if (leaderboard != null)
        {
            InitScrollView(leaderboard);
        }
        else
        {
            Debug.LogError("LeaderboardPopup | Init | Failed to read parameters");
        }
        return Task.CompletedTask;
    }

    /// <summary>
    /// Populates scroll view with new items of leaderboard entities
    /// </summary>
    /// <param name="leaderboard">List of leaderboard entities for populating scroll view</param>
    private void InitScrollView(List<LeaderboardEntity> leaderboard)
    {
        foreach (LeaderboardEntity entity in leaderboard)
        {
            GameObject entityObject = Instantiate(_leaderboardEntity, _entitiesContainer);
            LeaderboardScrollEntity leaderboardEntity = entityObject.GetComponent<LeaderboardScrollEntity>();
            leaderboardEntity.Init(entity);
        }
    }

    /// <summary>
    /// Loads prefab of leaderboard entity from resources for instantiating it and saving it in field of class
    /// </summary>
    private void LoadLeaderboardEntity()
    {
        GameObject prefab = Resources.Load<GameObject>("Prefabs/" + _leaderboardEntityPrefabName);
        if (prefab == null)
        {
            Debug.LogError("LeaderboardPopup | LoadLeaderboardEntity | No such reference: " + _leaderboardEntityPrefabName);
        }
        _leaderboardEntity = prefab;
    }

    /// <summary>
    /// Closes the popup
    /// </summary>
    public void ClosePopup()
    {
        Singleton<GameManager>.Instance.PopupManager.ClosePopup(_prefabName);
    }
}