using UnityEngine;

/// <summary>
/// Handles common interacting with UI canvas
/// </summary>
public class HUDController : MonoBehaviour
{
    /// <summary>
    /// Opens leaderboard popup and attaches it to canvas
    /// </summary>
    public void OpenLeaderboardPopup()
    {
        Singleton<GameManager>.Instance.ShowLeaderboardsDialog(gameObject.transform); 
    }
}