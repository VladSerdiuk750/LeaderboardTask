using SimplePopupManager;
using UnityEngine;

/// <summary>
/// Manages common game flow of the game and access to managers
/// </summary>
public class GameManager : Singleton<GameManager>
{
    /// <summary>
    /// Manager that handles interactions with popup
    /// </summary>
    public IPopupManagerService PopupManager { get; private set; }

    /// <summary>
    /// Manager that handles loading data for leaderboard
    /// </summary>
    public ILeaderboardManager LeaderboardManager { get; private set; }

    /// <summary>
    /// Manager that handles loading texture from internet
    /// </summary>
    public INetworkManager NetworkManager { get; private set; }

    /// <summary>
    /// Init GameManager and managers inside
    /// </summary>
    public GameManager()
    {
        PopupManager = new PopupManagerServiceService();
        LeaderboardManager = new LeaderboardManager();
        NetworkManager = new NetworkManager(this);
    }

    /// <summary>
    /// Load entities from json and opens leaderboard popup with its data
    /// </summary>
    /// <param name="parent">Parent to which it attaches to</param>
    public void ShowLeaderboardsDialog(Transform parent)
    {
        PopupManager.OpenPopup("LeaderboardPopup", parent, LeaderboardManager.LoadLeaderboardsEntities());
    }
}