using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INetworkManager
{
    /// <summary>
    /// Loading sprite from internet and return it by OnLoadedSuccess
    /// </summary>
    /// <param name="urlTexture">URL of texture</param>
    /// <param name="onLoadedSuccess">Invokes when sprite is loaded and returns it through it</param>
    /// <param name="onLoadedFail">Invokes when loading failes and returns error message</param>
    void OnLoadTexture(string urlTexture, Action<Sprite> onLoadedSuccess, Action<string> onLoadedFail = null);
}
