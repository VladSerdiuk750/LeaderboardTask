using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkManager : INetworkManager
{
    /// <summary>
    /// Monobehaviour to start a coroutine
    /// </summary>
    private readonly MonoBehaviour monoBehaviour;

    /// <summary>
    /// Inites network manager with coroutine starter
    /// </summary>
    /// <param name="coroutineStarter">Monobehaviour for starting coroutine</param>
    public NetworkManager(MonoBehaviour coroutineStarter)
    {
        monoBehaviour = coroutineStarter;
    }

    /// <summary>
    /// Loading sprite from internet and return it by OnLoadedSuccess
    /// </summary>
    /// <param name="urlTexture">URL of texture</param>
    /// <param name="onLoadedSuccess">Invokes when sprite is loaded and returns it through it</param>
    /// <param name="onLoadedFail">Invokes when loading failes and returns error message</param>
    public void OnLoadTexture(string urlTexture, Action<Sprite> onLoadedSuccess, Action<string> onLoadedFail = null)
    {
        OnSendRequest(urlTexture, (Texture2D texture) =>
        {
            onLoadedSuccess(ConvertTextureToSprite(texture));
        }, onLoadedFail);
    }

    /// <summary>
    /// Starting coroutine to request an image through url
    /// </summary>
    /// <param name="urlTexture">URL of texture</param>
    /// <param name="onLoadedSuccess">Invokes when sprite is loaded and returns it through it</param>
    /// <param name="onLoadedFail">Invokes when loading failes and returns error message</param>
    private void OnSendRequest(string urlTexture, Action<Texture2D> onLoadedSuccess, Action<string> onLoadedFail)
    {
        UnityWebRequest texture = UnityWebRequestTexture.GetTexture(urlTexture, nonReadable: false);
        monoBehaviour.StartCoroutine(RequestRoutine(texture, onLoadedSuccess, onLoadedFail));
    }

    /// <summary>
    /// Coroutine that waits for texture to be loaded and calls receivers
    /// </summary>
    /// <param name="textureRequest">Request that handles loading</param>
    /// <param name="onLoadedSuccess">Invokes when sprite is loaded and returns it through it</param>
    /// <param name="onLoadedFail">Invokes when loading failes and returns error message</param>
    /// <returns></returns>
    private IEnumerator RequestRoutine(UnityWebRequest textureRequest, Action<Texture2D> onLoadedSuccess, Action<string> onLoadedFail)
    {
        textureRequest.SendWebRequest();
        while (!textureRequest.isDone)
        {
            yield return new WaitForEndOfFrame();
        }
        CallReceivers(textureRequest, onLoadedSuccess, onLoadedFail);
    }

    /// <summary>
    /// Calls success or fail depending on result and dispose request
    /// </summary>
    /// <param name="textureRequest">Request that handles loading</param>
    /// <param name="onLoadedSuccess">Invokes when sprite is loaded and returns it through it</param>
    /// <param name="onLoadedFail">Invokes when loading failes and returns error message</param>
    private void CallReceivers(UnityWebRequest textureRequest, Action<Texture2D> onLoadedSuccess, Action<string> onLoadedFail)
    {
        if (IsReceiverSuccess(textureRequest))
        {
            onLoadedSuccess(DownloadHandlerTexture.GetContent(textureRequest));
        }
        else
        {
            onLoadedFail(textureRequest.error);
        }
        textureRequest.Dispose();
    }

    /// <summary>
    /// Check whether request succeded
    /// </summary>
    /// <param name="textureRequest">Request that handles loading</param>
    /// <returns>Whether request is succeded</returns>
    private bool IsReceiverSuccess(UnityWebRequest textureRequest)
    {
        if (textureRequest.responseCode >= 200 && textureRequest.responseCode < 300)
        {
            return !(textureRequest.result == UnityWebRequest.Result.ConnectionError);
        }
        return false;
    }

    /// <summary>
    /// Converts texture to sprite
    /// </summary>
    /// <param name="texture">Texture to convert</param>
    /// <returns>Sprite after converting</returns>
    private Sprite ConvertTextureToSprite(Texture2D texture)
    {
        Texture2D texture2D = new Texture2D(100, 100, TextureFormat.ETC_RGB4, mipChain: false);
        return Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f, 0u, SpriteMeshType.FullRect);
    }
}
