using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardManager : ILeaderboardManager
{
    /// <summary>
    /// Name of the data json file containing leadeboard entities in resources
    /// </summary>
    private string _leaderboardPath = "Leaderboard";

    /// <summary>
    /// Loads leaderboards entities from json
    /// </summary>
    /// <returns>List of leaderboard entities</returns>
    public List<LeaderboardEntity> LoadLeaderboardsEntities()
    {
        TextAsset leaderboardsJson = Resources.Load<TextAsset>(_leaderboardPath);
        return JsonUtility.FromJson<Leaderboard>(leaderboardsJson.text).leaderboard;
    }
}
