using UnityEngine;

/// <summary>
/// Constants for different entity types
/// </summary>
public static class LeaderboardScrollEntityConstants
{
    public static float defaultEntityWidth = 50f;

    public static float bronzeEntityWidth = 60f;

    public static float silverEntityWidth = 65f;

    public static float goldEntityWidth = 75f;

    public static float diamondEntityWidth = 85f;

    public static Color defaultColor = Color.gray;

    public static Color bronzeColor = new Color(0.804f, 0.498f, 0.196f);

    public static Color silverColor = new Color(0.753f, 0.753f, 0.753f);

    public static Color goldColor = new Color(1f, 0.843f, 0);

    public static Color diamondColor = new Color(0.725f, 0.949f, 1);
}
