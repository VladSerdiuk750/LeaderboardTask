using System;

[Serializable]
public class LeaderboardEntity
{
    public string name;
    public int score;
    public string avatar;
    public string type;
}
