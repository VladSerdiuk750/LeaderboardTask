using System.Collections.Generic;

public interface ILeaderboardManager
{
    /// <summary>
    /// Loads leaderboards entities from json
    /// </summary>
    /// <returns>List of leaderboard entities</returns>
    List<LeaderboardEntity> LoadLeaderboardsEntities();
}
