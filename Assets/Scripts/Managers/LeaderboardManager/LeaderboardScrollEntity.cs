using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Applies data to visual components in UI
/// </summary>
public class LeaderboardScrollEntity : MonoBehaviour
{
    /// <summary>
    /// Text component for name of the player
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI _name;

    /// <summary>
    /// Text component for score of the player
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI _score;

    /// <summary>
    /// Text component for avatar of the player
    /// </summary>
    [SerializeField]
    private Image _avatarImage;

    /// <summary>
    /// Label to show when loading image from the internet
    /// </summary>
    [SerializeField]
    private GameObject _avatarLoadingLabel;

    /// <summary>
    /// Image that controll color of background
    /// </summary>
    [SerializeField]
    private Image _backgroundColor;

    /// <summary>
    /// Initiates entity and populates text with data
    /// </summary>
    /// <param name="leaderboardEntity">Data for populating leaderboard entity</param>
    public void Init(LeaderboardEntity leaderboardEntity)
    {
        _name.text = leaderboardEntity.name;
        _score.text = leaderboardEntity.score.ToString();
        LoadTextureFromUrl(leaderboardEntity.avatar);
        LeaderboardEntityType type = ConvertToEnumHelper.ToEnum(leaderboardEntity.type, LeaderboardEntityType.Default);
        AdaptToLeaderboardEntityType(type);
    }

    /// <summary>
    /// Start loading texture for avatar
    /// </summary>
    /// <param name="url">URL of image</param>
    private void LoadTextureFromUrl(string url)
    {
        Singleton<GameManager>.Instance.NetworkManager.OnLoadTexture(url, OnLoadedSuccess, OnLoadedFailed);
    }

    /// <summary>
    /// Apply sprite to avatar and hides loading label
    /// </summary>
    /// <param name="spriteFromUrl">Sprite that will be applied</param>
    private void OnLoadedSuccess(Sprite spriteFromUrl)
    {
        _avatarLoadingLabel.SetActive(false);
        _avatarImage.sprite = spriteFromUrl;
    }

    /// <summary>
    /// Writes error in console when loaded fails
    /// </summary>
    /// <param name="errorMessage">Message for the console</param>
    private void OnLoadedFailed(string errorMessage)
    {
        Debug.LogError(errorMessage);

    }

    /// <summary>
    /// Adapting size and color of entity depending on type
    /// </summary>
    /// <param name="type">Type of leaderboard entity</param>
    private void AdaptToLeaderboardEntityType(LeaderboardEntityType type)
    {
        RectTransform rect = GetComponent<RectTransform>();
        switch(type)
        {
            case LeaderboardEntityType.Default:
                {
                    rect.sizeDelta = new Vector2(rect.sizeDelta.x, LeaderboardScrollEntityConstants.defaultEntityWidth);
                    _backgroundColor.color = LeaderboardScrollEntityConstants.defaultColor;
                    break;
                }
            case LeaderboardEntityType.Bronze:
                {
                    rect.sizeDelta = new Vector2(rect.sizeDelta.x, LeaderboardScrollEntityConstants.bronzeEntityWidth);
                    _backgroundColor.color = LeaderboardScrollEntityConstants.bronzeColor;
                    break;
                }
            case LeaderboardEntityType.Silver:
                {
                    rect.sizeDelta = new Vector2(rect.sizeDelta.x, LeaderboardScrollEntityConstants.silverEntityWidth);
                    _backgroundColor.color = LeaderboardScrollEntityConstants.silverColor;
                    break;
                }
            case LeaderboardEntityType.Gold:
                {
                    rect.sizeDelta = new Vector2(rect.sizeDelta.x, LeaderboardScrollEntityConstants.goldEntityWidth);
                    _backgroundColor.color = LeaderboardScrollEntityConstants.goldColor;
                    break;
                }
            case LeaderboardEntityType.Diamond:
                {
                    rect.sizeDelta = new Vector2(rect.sizeDelta.x, LeaderboardScrollEntityConstants.diamondEntityWidth);
                    _backgroundColor.color = LeaderboardScrollEntityConstants.diamondColor;
                    break;
                }
            default: break;
        }
    }
}

/// <summary>
/// Enum for differentiating entity types
/// </summary>
public enum LeaderboardEntityType
{
    Default,
    Bronze,
    Silver,
    Gold,
    Diamond
}